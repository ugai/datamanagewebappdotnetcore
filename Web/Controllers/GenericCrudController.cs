using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;
using Persistence;
using Persistence.Model;

namespace Web.Controllers
{
    public class PeopleController : GenericCrudController<Person>
    {
        public PeopleController(HogeContext context) : base(context) { }
    }

    public class AlcholDrinkTypesController : GenericCrudController<alchol_drink_type>
    {
        public AlcholDrinkTypesController(HogeContext context) : base(context) { }
    }

    /// <summary>
    /// 汎用 CRUD コントローラ
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class GenericCrudController<T> : Controller
        where T : class, IEntity, new()
    {
        private readonly HogeContext _context;
        private readonly string _routeName = "Views/GenericCrud";
        private readonly string _modelName = typeof(T).Name;
        private readonly List<string> _readOnlyPropertyNames = new List<string>()
        {
            "Id",
            "Created",
            "Updated",
        };

        public GenericCrudController(HogeContext context)
        {
            _context = context;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);
            ViewData["ModelDisplayName"] = _modelName;
            ViewBag.ReadOnlyPropertyNames = _readOnlyPropertyNames;
        }

        // GET: {T}
        public async Task<IActionResult> Index()
        {
            var item = await _context.Set<T>().ToListAsync();
            return View($"~/{_routeName}/Index.cshtml", item);
        }

        // GET: {T}/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _context.Set<T>()
                .SingleOrDefaultAsync(m => m.Id == id);
            if (item == null)
            {
                return NotFound();
            }

            return View($"~/{_routeName}/Details.cshtml", item);
        }

        // GET: {T}/Create
        public IActionResult Create()
        {
            var item = new T();
            return View($"~/{_routeName}/Create.cshtml", item);
        }

        // POST: {T}/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(T item)
        {
            if (ModelState.IsValid)
            {
                _context.Add<T>(item);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View($"~/{_routeName}/Create.cshtml", item);
        }

        // GET: {T}/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _context.Set<T>().SingleOrDefaultAsync(m => m.Id == id);
            if (item == null)
            {
                return NotFound();
            }

            return View($"~/{_routeName}/Edit.cshtml", item);
        }

        // POST: {T}/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int? id, T item)
        {
            if (id != item.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(item);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ItemExists(item.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View($"~/{_routeName}/Edit.cshtml", item);
        }

        // GET: {T}/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _context.Set<T>()
                .SingleOrDefaultAsync(m => m.Id == id);
            if (item == null)
            {
                return NotFound();
            }

            return View($"~/{_routeName}/Delete.cshtml", item);
        }

        // POST: {T}/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int? id)
        {
            var item = await _context.Set<T>().SingleOrDefaultAsync(m => m.Id == id);
            item.Removed = true;
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool ItemExists(int? id)
        {
            return _context.Set<T>().Any(e => e.Id == id);
        }
    }
}

﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Web.TagHelpers
{
    /// <summary>
    /// based on: https://stackoverflow.com/questions/34282640/how-can-i-pass-string-value-for-asp-for-in-asp-net-5
    /// </summary>
    [HtmlTargetElement("edit")]
    public class EditTagHelper : TagHelper
    {
        [HtmlAttributeName("asp-for")]
        public ModelExpression aspFor { get; set; }

        [ViewContext]
        [HtmlAttributeNotBound]
        public ViewContext ViewContext { get; set; }

        protected IHtmlGenerator _generator { get; set; }

        public EditTagHelper(IHtmlGenerator generator)
        {
            _generator = generator;
        }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            var propName = aspFor.ModelExplorer.Model.ToString();
            var modelExProp = aspFor.ModelExplorer.Container.Properties.Single(x => x.Metadata.PropertyName.Equals(propName));
            var propValue = modelExProp.Model;
            var propEditFormatString = modelExProp.Metadata.EditFormatString;
            var propType = modelExProp.Model?.GetType();

            var label = _generator.GenerateLabel(ViewContext, aspFor.ModelExplorer,
                propName, propName, new { @class = "col-md-2 control-label", @type = "text" });

            TagBuilder input;
            if (propType == typeof(bool))
            {
                input = _generator.GenerateCheckBox(ViewContext, aspFor.ModelExplorer,
                    propName, (bool)propValue, null);
            }
            else
            {
                string typeAttr = string.Empty;

                if (propType == typeof(int))
                {
                    typeAttr = "number";
                }
                else if (propType == typeof(DateTime))
                {
                    typeAttr = "datetime-local";
                }

                input = _generator.GenerateTextBox(ViewContext, aspFor.ModelExplorer,
                    propName, propValue, propEditFormatString, new { @class = "form-control", @type = typeAttr });
            }

            var validation = _generator.GenerateValidationMessage(ViewContext, modelExProp, propName,
                string.Empty, string.Empty, new { @class = "text-danger" });

            var inputParent = new TagBuilder("div");
            inputParent.AddCssClass("col-md-10");
            inputParent.InnerHtml.AppendHtml(input);
            inputParent.InnerHtml.AppendHtml(validation);

            var parent = new TagBuilder("div");
            parent.AddCssClass("form-group");
            parent.InnerHtml.AppendHtml(label);
            parent.InnerHtml.AppendHtml(inputParent);

            output.Content.SetHtmlContent(parent);
            base.Process(context, output);
        }
    }
}
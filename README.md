# about

ASP.NET Core と EF Core を使ったデータ管理サイトのサンプル。
ライセンスは WTFPL (勝手にしてください)。


# 使い方
## ローカル

1. Persistence.HogeContext.localConncetionString の設定で接続可能な MySQL データベースを用意。
2. Web プロジェクトをデバッグ実行。


## Azure

1. Web プロジェクトを発行
2. Azure の Web ダッシュボードから「MySQL In App」を有効化


# EF Core のマイグレーション

マイグレーションの関連の操作はVisual Studio のパッケージマネージャーコンソールから行う。
「既定のプロジェクト」を Persistence にして下記のようなコマンドを実行する。

```
   Add-Migration -StartupProject Persistence -Context HogeContext AddHogeHogeDataModel
   Update-Database -StartupProject Persistence -Context HogeContext
```


# 参考にしたサイト

* https://docs.microsoft.com/ja-jp/aspnet/core/
* https://docs.microsoft.com/ja-jp/ef/core/



[ugai](https://bitbucket.org/ugai/)

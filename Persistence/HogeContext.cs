﻿namespace Persistence
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using Microsoft.EntityFrameworkCore;
    using Persistence.Model;

    /// <summary>
    /// サンプルコンテキスト
    /// </summary>
    /// <remarks>
    /// </remarks>
    public class HogeContext : DbContext
    {
        private readonly string localConncetionString = @"Server=localhost;database=ef;uid=root;pwd=hogehoge;";

        public DbSet<Person> Persons { get; set; }
        public DbSet<alchol_drink_type> alchol_drink_type { get; set; }

        /// <inheritdoc/>
        protected override void OnConfiguring(DbContextOptionsBuilder optionBuilder)
        {
            optionBuilder
                .UseMySql(
                    GetAzureMySqlInAppConnectionString() ??
                    this.localConncetionString)
                ;
        }

        /// <inheritdoc/>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>((e) =>
            {
                e.Property(x => x.Created).HasDefaultValueSql("CURRENT_TIMESTAMP(6)");
                e.Property(x => x.Updated).HasComputedColumnSql("CURRENT_TIMESTAMP(6)");
            });

            modelBuilder.Entity<alchol_drink_type>((e) =>
            {
                e.Property(x => x.Created).HasDefaultValueSql("CURRENT_TIMESTAMP(6)");
                e.Property(x => x.Updated).HasComputedColumnSql("CURRENT_TIMESTAMP(6)");
            });
        }

        /// <summary>
        /// Azure MySQL in-app for Web Apps 用
        /// https://blogs.msdn.microsoft.com/appserviceteam/2016/08/18/announcing-mysql-in-app-preview-for-web-apps/
        /// </summary>
        private static string GetAzureMySqlInAppConnectionString()
        {
            var s = Environment.GetEnvironmentVariable("MYSQLCONNSTR_localdb") ?? string.Empty;

            var hostPort = Regex.Match(s, @"^.*Data Source=(.+?);.*$").Groups[1].Value;
            var host = hostPort.Split(':').FirstOrDefault();
            var port = hostPort.Split(':').LastOrDefault();
            var db = Regex.Match(s, @"^.*Database=(.+?);.*$").Groups[1].Value;
            var username = Regex.Match(s, @"^.*User Id=(.+?);.*$").Groups[1].Value;
            var password = Regex.Match(s, @"^.*Password=(.+?)$").Groups[1].Value;



            var options = new List<string>() { host, port, db, username, password };
            if (options.Any(x => string.IsNullOrWhiteSpace(x)))
            {
                return null;
            }

            return $@"server={host};userid={username};password={password};database={db};Port={port}";
        }
    }
}

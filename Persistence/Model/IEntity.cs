﻿namespace Persistence.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// 要素
    /// </summary>
    public interface IEntity
    {
        /// <summary>
        /// Gets or sets Id
        /// </summary>
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        int? Id { get; set; }

        /// <summary>
        /// Gets or sets 作成日時
        /// </summary>
        [Required]
        DateTime Created { get; set; }

        /// <summary>
        /// Gets or sets 更新日時
        /// </summary>
        [Required]
        DateTime Updated { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether 論理削除済み
        /// </summary>
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        bool Removed { get; set; }
    }
}

﻿namespace Persistence.Model
{
    using System;

    /// <summary>
    /// 人
    /// </summary>
    public class Person : IEntity
    {
        /// <inheritdoc/>
        public int? Id { get; set; }

        /// <summary>
        /// Gets or sets 名前
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets 名字
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets 年齢
        /// </summary>
        public int Age { get; set; }

        /// <inheritdoc/>
        public DateTime Created { get; set; }

        /// <inheritdoc/>
        public DateTime Updated { get; set; }

        /// <inheritdoc/>
        public bool Removed { get; set; }
    }
}

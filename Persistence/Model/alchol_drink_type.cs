﻿namespace Persistence.Model
{
    using System;

    /// <summary>
    /// 酒
    /// </summary>
    public class alchol_drink_type : IEntity
    {
        /// <inheritdoc/>
        public int? Id { get; set; }

        /// <summary>
        /// Gets or sets 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets アルコール度数
        /// </summary>
        public int AlcholContent { get; set; }

        /// <inheritdoc/>
        public DateTime Created { get; set; }

        /// <inheritdoc/>
        public DateTime Updated { get; set; }

        /// <inheritdoc/>
        public bool Removed { get; set; }
    }
}

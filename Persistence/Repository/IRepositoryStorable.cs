﻿namespace Persistence.Repository
{
    /// <summary>
    /// 書き込みインタフェース
    /// </summary>
    /// <typeparam name="T">読み取る値</typeparam>
    public interface IRepositoryStorable<T>
    {
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="item">要素</param>
        void Store(T item);
    }
}

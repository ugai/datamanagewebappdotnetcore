﻿namespace Persistence.Repository
{
    using Persistence.Model;

    /// <summary>
    /// Person リポジトリ
    /// </summary>
    public class PersonRepository : RepositoryBase<Person>
    {
    }
}

﻿namespace Persistence.Repository
{
    using System;
    using System.Linq;
    using Persistence.Model;

    /// <summary>
    /// リポジトリインタフェース
    /// </summary>
    /// <typeparam name="T">要素</typeparam>
    public abstract class RepositoryBase<T> : IRepositoryFetchable<T>, IRepositoryStorable<T>
        where T : class, IEntity
    {
        /// <inheritdoc/>
        public T Fetch(int id)
        {
            using (var ctx = new HogeContext())
            {
                return ctx.Set<T>().FirstOrDefault(x => x.Id == id);
            }
        }

        /// <inheritdoc/>
        public void Store(T newItem)
        {
            if (newItem == null)
            {
                throw new ArgumentNullException("newItem");
            }

            using (var ctx = new HogeContext())
            {
                if (newItem.Id.HasValue)
                {
                    var oldItem = this.Fetch(newItem.Id.Value);
                    var result = ctx.Set<T>().Update(newItem);
                    result.Entity.Created = oldItem.Created;
                }
                else
                {
                    ctx.Set<T>().Add(newItem);
                }

                ctx.SaveChanges();
            }
        }
    }
}

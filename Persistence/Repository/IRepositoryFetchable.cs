﻿namespace Persistence.Repository
{
    /// <summary>
    /// 読み取りインタフェース
    /// </summary>
    /// <typeparam name="T">読み取る値</typeparam>
    public interface IRepositoryFetchable<T>
    {
        /// <summary>
        /// 取得
        /// </summary>
        /// <param name="id">キー</param>
        /// <returns>要素</returns>
        T Fetch(int id);
    }
}
